package com.comedybear.tommabe;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class Util {

	protected static synchronized String getUrlContent(String url) throws Exception {
		  
        String ret = "";
         
        try {
             
            URL u = new URL(url+"?appid="+MainActivity.APP_ID);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setDefaultUseCaches(true);
            c.setUseCaches(true);
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
             
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(c.getInputStream()));
  
            ret = rd.readLine();
             
            rd.close();
            c.disconnect();
             
            return ret;
             
        } catch (IOException e) {
            throw new Exception("Network Error", e);
        }
    }
	
	public Bitmap getRemoteImage(URL aURL) {
	    try {
	        URLConnection conn = aURL.openConnection();
	        conn.connect();
	        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
	        Bitmap bm = BitmapFactory.decodeStream(bis);
	        bis.close();
	        return bm;
	    } catch (IOException e) {
	        Log.d("getRemoteImage", "Error fetching bitmap!: "+aURL);
	    }
	    return null;
	}
	
	
}
