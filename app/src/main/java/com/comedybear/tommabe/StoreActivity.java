package com.comedybear.tommabe;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class StoreActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		 setContentView(R.layout.store);
		 
		 ImageView bBack = (ImageView)findViewById(R.id.bBack);
		 bBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				StoreActivity.this.finish();
			}
		});
		 
		 LinearLayout l1 = (LinearLayout)findViewById(R.id.cTd);
		 
		 ImageView footerBox = (ImageView)findViewById(R.id.footerBox);
		 
		 footerBox.getDrawable().setDither(true);
		 footerBox.getDrawable().setFilterBitmap(true);
		 footerBox.invalidate();
		 
		LayoutInflater inflater = (LayoutInflater)this.getSystemService
				      (Context.LAYOUT_INFLATER_SERVICE);
		 
		 for(int i=0;i<5;i++)
		 {
			 RelativeLayout tmpRow = (RelativeLayout)inflater.inflate(R.layout.store_row, null);
			 
			 l1.addView(tmpRow);
		 }
		 
	}
	
}
