package com.comedybear.tommabe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class VideoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		 setContentView(R.layout.videos);
		 
		 LayoutInflater inflater = (LayoutInflater)this.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);
		 
		 ImageView bBack = (ImageView)findViewById(R.id.bBack);
		 bBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				VideoActivity.this.finish();
			}
		});
	
		
		RelativeLayout rl = (RelativeLayout)findViewById(R.id.cTd);
		 
		 int x = 1;
		 int heightOffset = 0;
		 
		 for(int i=0;i<11;i++)
		 {
			 RelativeLayout tmpRow = (RelativeLayout)inflater.inflate(R.layout.video_row, null);
			 
			 RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					 															RelativeLayout.LayoutParams.WRAP_CONTENT);
			 
			 switch(x)
			 {
			 	case(1):
			 		lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			 		lp.setMargins(0, heightOffset, 0, 0);
			 		tmpRow.setLayoutParams(lp);
			 		x = 2;
			 		break;
			 	case(2):
			 		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			 		lp.setMargins(0, heightOffset, 0, 0);
		 			tmpRow.setLayoutParams(lp);
		 			
		 			heightOffset += 125;
		 			x = 1;
			 		break;
			 	
			 }
			 
			 
			 tmpRow.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse("http://www.youtube.com/watch?v=XhAmHuNyxSY"));
					startActivity(i);
				}
			});
			 
			 rl.addView(tmpRow);
		 }
		
		 rl.invalidate();
		 
	}
}
