package com.comedybear.tommabe;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SoundActivity extends Activity {

	
	private static Sound selectedSound = null;
	private LayoutInflater inflater;
	private Handler mHandler = new Handler();
	private LinearLayout cTd;
	private static SoundPlayer soundPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 setContentView(R.layout.sounds);
		 
		 ImageView bBack = (ImageView)findViewById(R.id.bBack);
		 bBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SoundActivity.this.finish();
			}
		});
		 
		 inflater = (LayoutInflater)this.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);
		 
		 cTd = (LinearLayout)findViewById(R.id.cTd);
		 soundPlayer = new SoundPlayer(this);
		 
		 new Thread(new Runnable() {
			public void run() {
				initLayout();
			}
		}).start();
		 
	}
	
	public void initLayout()
    {
		for(int i =0;i<25;i++){
			mHandler.post(new Runnable() {
				public void run() {
					RelativeLayout tmpRow = (RelativeLayout)inflater.inflate(R.layout.sound_row, null);
					
					tmpRow.setTag(new Integer(0));
					tmpRow.setOnClickListener(buttonClick);
					tmpRow.setOnLongClickListener(buttonLongClick);
					tmpRow.setOnTouchListener(buttonTouch);
					
					ImageView bPlay = (ImageView)tmpRow.findViewById(R.id.bPlay);
			        bPlay.setOnClickListener(ButtonClickListener);
					
			        Sound s = new Sound("sound1","Ubar sound");
			        
			        TextView bText = (TextView)tmpRow.findViewById(R.id.title);
			        bText.setTag(s);
			        
					cTd.addView(tmpRow);
				}
			});
		}
    }
	
	public static View.OnClickListener ButtonClickListener = new View.OnClickListener()
	{
		
		public void onClick(View v) {
			try{
				RelativeLayout rl = (RelativeLayout)v.getParent();
				TextView buttonText = (TextView)rl.findViewById(R.id.title);
				Sound s = (Sound)buttonText.getTag();
				
				Integer tmpTag = (Integer) rl.getTag();
				boolean isPlaying = (tmpTag==1)?true:false;
				
				if(!buttonText.isFocused())
					buttonText.requestFocus();
				
				if(!buttonText.isSelected())
					buttonText.setSelected(true);
				
				if(!isPlaying){
					rl.setTag(new Integer(1));
					soundPlayer.playSound(s.id, v);
				}
				else {
					soundPlayer.stop();
					rl.setTag(new Integer(0));
				}
				
				rl = null;
				buttonText = null;
			} catch(Exception e){
				
			}
		}
	};
	
	public static View.OnClickListener buttonClick = new View.OnClickListener() {
		
		public void onClick(View v) {
			TextView tv = (TextView)v.findViewById(R.id.title);
			tv.requestFocus();
			if(!tv.isSelected())
				tv.setSelected(true);
		}
	};
	
	
	public static View.OnFocusChangeListener buttonMouseOut = new View.OnFocusChangeListener() {
		
		public void onFocusChange(View v, boolean hasFocus) {
			if(!hasFocus){
				v.setSelected(false);
				v.clearFocus();
			}
			/*if(hasFocus)
			{
				if(!v.isSelected()){
					v.playSoundEffect(SoundEffectConstants.CLICK);
				}
				selectedSound = (Sound)v.getTag();
			} else{
			}
			
			v.setSelected(hasFocus);*/
		}
	};
	
	public static View.OnTouchListener buttonTouch = new View.OnTouchListener() {
		
		public boolean onTouch(View v, MotionEvent event) {

			if(event.getAction()==MotionEvent.ACTION_MOVE){
				return true;
	    	} else if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
	    		//Log.e("sb3","got button motion event: outside");
	    	} else if(event.getAction()==MotionEvent.ACTION_UP){
				
	    		long elapsed = event.getEventTime()-event.getDownTime();
	    		if(elapsed>600)
	    			return false;
	    		
	    		TextView tv = (TextView)v.findViewById(R.id.title);
	    		
	    		if(!tv.isSelected()){
	    			tv.playSoundEffect(SoundEffectConstants.CLICK);
	    			tv.setSelected(true);
				}
				selectedSound = (Sound)tv.getTag();
	    		//Log.e("sb3","got button motion event: up");
	    	} else if(event.getAction()==MotionEvent.ACTION_DOWN){
	    		//Log.e("sb3","got button motion event: down");
	    	}
			return false;
		}
	};
	
	public static View.OnTouchListener textTouch = new View.OnTouchListener() {
		
		public boolean onTouch(View v, MotionEvent event) {

			if(event.getAction()==MotionEvent.ACTION_MOVE){
				//v.clearFocus();
				return true;
	    	} else if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
	    		v.setSelected(false);
	    		//Log.e("sb3","got text motion event: outside");
	    	} else if(event.getAction()==MotionEvent.ACTION_UP){
				
	    		long elapsed = event.getEventTime()-event.getDownTime();
	    		if(elapsed>750)
	    			return false;
	    		
	    		if(!v.isSelected()){
					v.playSoundEffect(SoundEffectConstants.CLICK);
					v.setSelected(true);
					v.requestFocus();
				}
				selectedSound = (Sound)v.getTag();
	    		//Log.e("sb3","got text motion event: up");
	    	} else if(event.getAction()==MotionEvent.ACTION_DOWN){
	    		//Log.e("sb3","got text motion event: down");
	    	}
			return false;
		}
	};
	
	public static View.OnLongClickListener buttonLongClick = new View.OnLongClickListener() {
		
		public boolean onLongClick(View v) {
			TextView tv = (TextView)v.findViewById(R.id.title);
			
			tv.setSelected(true);
			tv.requestFocus();
			
			selectedSound = (Sound)tv.getTag();
			
			//tv.playSoundEffect(SoundEffectConstants.CLICK);
			//((Activity) ctx).showDialog(DLG_OPTIONS);
			return true;
		}
	};
	
	public static View.OnLongClickListener textLongClick = new View.OnLongClickListener() {
		
		public boolean onLongClick(View v) {
			
			/*if(!v.isSelected()){
				v.setSelected(true);
			}*/
			
			selectedSound = (Sound)v.getTag();
			
			v.setSelected(true);
			v.requestFocus();
			
			//v.playSoundEffect(SoundEffectConstants.CLICK);
			//((Activity) ctx).showDialog(DLG_OPTIONS);
			return true;
		}
	};
	
}
