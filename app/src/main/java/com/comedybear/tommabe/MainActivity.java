package com.comedybear.tommabe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity {
	
	public final static String APP_ID = "123";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        LinearLayout ll = (LinearLayout)findViewById(R.id.MenuView);
        ll.bringToFront();
        
        
        Button b1 = (Button)findViewById(R.id.bTourdate);
        b1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this,TourdateActivity.class);
				startActivity(i);
			}
		});
        
        Button b2 = (Button)findViewById(R.id.bStore);
        b2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this,StoreActivity.class);
				startActivity(i);
			}
		});
        
        Button b3 = (Button)findViewById(R.id.bVideos);
        b3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this,VideoActivity.class);
				startActivity(i);
			}
		});
        
        Button b4 = (Button)findViewById(R.id.bRingtones);
        b4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this,SoundActivity.class);
				startActivity(i);
			}
		});
    }
}