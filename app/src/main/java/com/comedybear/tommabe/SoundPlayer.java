package com.comedybear.tommabe;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SoundPlayer
{
	private Context ctx;
	
	private String currentId = null;
	private View currentView = null;
	private MediaPlayer mp = null;
	
	private static boolean dlgPaused = false;
	
	public SoundPlayer(Context c) {
		ctx = c;
	}
	
	public OnCompletionListener mpComplete = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			
			if(currentView!=null){
				currentView.setBackgroundResource(R.drawable.play);
				try
				{
					RelativeLayout rl = (RelativeLayout)currentView.getParent();
					rl.setTag(new Integer(0));
				} catch(Exception e){
					
				}
			}
			
			currentId = null;
			currentView = null;
			mp.release();
		}
	};
	
	public void stop() {
		if(mp!=null){
			try{
				if(mp.isPlaying()){
					try{
						mp.stop();
						mp.release();
						if(currentView!=null)
							currentView.setBackgroundResource(R.drawable.play);
					} catch(IllegalStateException e){
						mp.release();
						mp = null;
					}
				}
			} catch(Exception e){
				if(currentView!=null)
					currentView.setBackgroundResource(R.drawable.play);
			}
			
			RelativeLayout rl = (RelativeLayout)currentView.getParent();
			rl.setTag(new Integer(0));
			
			//currentView.setTag("1");
			currentId = null;
			currentView = null;
			rl = null;
		}
	}
	
	public void playSound(String name,View v){
		
		if(currentId!=null)
		{
			stop();
		}
		
		currentId = name;
		currentView = v;
		
		v.setBackgroundResource(R.drawable.stop);
		
		int resId = ctx.getResources().getIdentifier(currentId, "raw", ctx.getPackageName());
		
		if(resId!=0)
		{
			mp = MediaPlayer.create(ctx, resId);
			if(mp!=null){
				mp.setOnCompletionListener(mpComplete);
				mp.start();
			} else {
				Toast.makeText(ctx, "Error!", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	public void pause()
	{
		try{
			if(!dlgPaused && mp!=null && mp.isPlaying()){
				dlgPaused = true;
				mp.pause();
			}
		} catch (Exception e) {
		}
	}
	
	public void resume()
	{
		try{
			if(dlgPaused && !mp.isPlaying()){
				dlgPaused = false;
				mp.start();
			}
		} catch (Exception e) {
		}
	}
	
	public void destroy()
	{
		try{
			if(mp!=null){
				if(mp.isPlaying())
					mp.stop();
				mp.release();
			}
		} catch (Exception e) {
		}
	}

}
