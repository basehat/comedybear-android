package com.comedybear.tommabe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TourdateActivity extends Activity {

	private static final String TD_SERVER_URI = "/service/tourdates";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		 setContentView(R.layout.tourdates);
		 
		 ImageView bBack = (ImageView)findViewById(R.id.bBack);
		 bBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				TourdateActivity.this.finish();
			}
		});
		 
		 
		 
		 //show inline progress bar
		 
		// TextView t1 = (TextView)findViewById(R.id.tRowName);
		// t1.setEllipsize(TruncateAt.MARQUEE);
		// t1.requestFocus();
		 
		 bBack.post(new Runnable() {
			public void run() {
				fetchDates();
			}
		});
		 
	}
	
	protected void fetchDates()
	{
		Toast.makeText(this, "Fetching Latest Data From Server...", Toast.LENGTH_SHORT).show();
		String tmpUrl = getString(R.string.service_host_url) + TD_SERVER_URI;
		Log.w("TD Activity","Fetching dates from: "+tmpUrl);
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService
			      (Context.LAYOUT_INFLATER_SERVICE);
		
		try {
			String res = Util.getUrlContent(tmpUrl);
			Log.w("TD Activity","Got result: "+res);
			
			 LinearLayout tdList = (LinearLayout)findViewById(R.id.cTd);
			 
			 /*for(int i=0;i<6;i++){
				 LinearLayout tmpLayout = (LinearLayout)inflater.inflate(R.layout.tour_row, null);
				 
				 tdList.addView(tmpLayout);
			 }*/
			
			try {
				
				JSONArray responseArr = new JSONArray(res);
				
				
				for(int x=0;x<responseArr.length();x++)
				{
					JSONObject tdRow = responseArr.getJSONObject(x);
					
					Log.w("TD Activity","Got tour date: "+tdRow.getString("name"));
					
					
					LinearLayout tmpLayout = (LinearLayout)inflater.inflate(R.layout.tour_row, null);
					 
					TextView t = (TextView)tmpLayout.findViewById(R.id.tRowName);
					
					t.setText("- "+tdRow.getString("name"));
					
					tmpLayout.setTag(tdRow.getString("url"));
					
					tmpLayout.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							String targetUrl = (String) v.getTag();
							Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(targetUrl));
							startActivity(i);
						}
					});
					
					tdList.addView(tmpLayout);
					
					
				}
				
				
				tdList.invalidate();
				
			} catch (JSONException e) {
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
