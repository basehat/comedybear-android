package com.comedybear.tommabe;

class Sound{
	public String id;
	public String name;
	
	public Sound(String i,String n) {
		id = i;
		name = n;
	}
	
	public Sound() {
	}
	
	public Sound clone()
	{
		return new Sound(id,name);
	}
	
}
